#include <pcap.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <arpa/inet.h>

void usage() {
  printf("syntax: pcap_test <interface>\n");
  printf("sample: pcap_test wlan0\n");
}

int main(int argc, char* argv[]) {
  if (argc != 2) {
    usage();
    return -1;
  }

  char* dev = argv[1];
  char errbuf[PCAP_ERRBUF_SIZE];

  const int ether_len=14;
  const int mac_len=6;
  unsigned char dst_mac[mac_len]={0};
  unsigned char src_mac[mac_len]={0};

  const unsigned char ipv4[2]={0x08, 0x00};
  unsigned char ether_type[2]={0};

  char ip_header_len=0;
  const int src_ip_loc=0x1a;
  const int dst_ip_loc=0x1e;
  const int ip_len=4;
  unsigned char dst_ip[ip_len]={0};
  unsigned char src_ip[ip_len]={0};

  const int protocol_loc=0x17;
  const unsigned char tcp=0x06;
  unsigned char protocol=0;
  
  const int port_len=2;
  uint16_t dst_port=0;
  uint16_t src_port=0;

  const int data_offset_loc=0x0c;
  char tcp_header_len=0;

  char tot_header_len=0;

  const int MAX_PRINT=32;


  pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
  if (handle == NULL) {
    fprintf(stderr, "couldn't open device %s: %s\n", dev, errbuf);
    return -1;
  }

  while (true) {
    struct pcap_pkthdr* header;
    const u_char* packet;
    int res = pcap_next_ex(handle, &header, &packet);
    if (res == 0) continue;
    if (res == -1 || res == -2) break;

    printf("%u bytes captured\n", header->caplen);

    //get mac addresses
    memcpy(&dst_mac, packet, mac_len);
    memcpy(&src_mac, packet+mac_len, mac_len);
    printf("src mac : %02x:%02x:%02x:%02x:%02x:%02x\n", src_mac[0], src_mac[1], src_mac[2], src_mac[3], src_mac[4], src_mac[5]);
    printf("dst mac : %02x:%02x:%02x:%02x:%02x:%02x\n", dst_mac[0], dst_mac[1], dst_mac[2], dst_mac[3], dst_mac[4], dst_mac[5]);

    //check if IP
    memcpy(&ether_type, packet+mac_len*2, 2);
    if(!((ether_type[0]==ipv4[0])&&(ether_type[1]==ipv4[1]))) {printf("\n"); continue;};

    //calculate IP header length
    memcpy(&ip_header_len, packet+ether_len, 1);
    ip_header_len&=0x0f;
    ip_header_len*=4;

    //get IP addresses
    memcpy(&dst_ip, packet+dst_ip_loc, ip_len);
    memcpy(&src_ip, packet+src_ip_loc, ip_len);
    printf("src ip  : %d.%d.%d.%d\n", src_ip[0], src_ip[1], src_ip[2], src_ip[3]);
    printf("dst ip  : %d.%d.%d.%d\n", dst_ip[0], dst_ip[1], dst_ip[2], dst_ip[3]);

    //check if TCP
    memcpy(&protocol, packet+protocol_loc, 1);
    if(protocol!=tcp) {printf("\n");continue;};

    //get port numbers
    memcpy(&src_port, packet+(ether_len+ip_header_len), sizeof(uint16_t));
    memcpy(&dst_port, packet+(ether_len+ip_header_len+port_len), sizeof(uint16_t));
    printf("src port: %u\n", ntohs(src_port));
    printf("dst port: %u\n", ntohs(dst_port));

    //calculate TCP header length
    memcpy(&tcp_header_len, packet+(ether_len+ip_header_len+data_offset_loc), 1);
    tcp_header_len=((tcp_header_len & 0xf0) >> 4) * 4;

    tot_header_len=ether_len+ip_header_len+tcp_header_len;
    for(int i=0; ;i++)
    {
    	if(((tot_header_len+i+1)>(header->caplen))||i==MAX_PRINT) {printf("\n");break;}
    	else if(i!=0){
    		if(i%8==0) printf("\n");
    		else printf(" ");
    	};
    	printf("%02x", packet[tot_header_len+i]);
    }


    printf("\n");
  }

  pcap_close(handle);
  return 0;
}
